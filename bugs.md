- Thinking about - прямо сейчас думаю о
- Thinking of - подумываю о
- Successfully - Саксесфулли [ok] / Сацессфулли [нет]
- Dull (далл) - скучный
- Honest (онест) - честный
- Inhabitat - незаселённая, необитаемая
- Also (олсо) - также
- Either - тоже (в отрицании)
- It was well - не ок, It was quite well - ок.
- remember - я помню, about you - я помню про тебя, you - я помню тебя
- remind - напомнил, of calling you - напомнил позвонить тебе
- memorize - запомнить какие-то данные
- resemble - напомнил кого-то (you resemble my best friend)
- an hour - ок, a hour - не ок
- hardly ever - едва ли
- questing (квесчен) - вопрос, квешн - не ок.
- pleasant - довольный
- encourage - вдохновляет
- skate - кататься на коньках
- ordinary - о'динари
- amateur - э'мэтэр
- tomorrow in the evening - ok, tomorrow in evening - не ок
- vows / wedding vows - обещания
- recently - не now и не уходит в конец предложения
- fault - вина, косяк
- admit - признать
- факты, регулярно - if present simple, present simple
- планы на будущее - if present simple, future simple
- если бы, додумки - if past simple, would V
- если бы я был на твоём месте - if I were you, I would do it.
- is looking like - не ок
- looks like - ок
- cloudly - облачно
- as I remember - не ок
- as far as I remember - ок
- understanding - только в качестве "понимание", но никак не глагол
- for years - является признаком того, что это предложение с фактом
- punctual - панктуал - ок, панкчуал - не ок
- exceptionally well - исключительно хорошо
- sociable - сошэбл ок, сошиэбл - не ок, социэйбл - не ок
- appreciate - ценят
- reliable - релайбл ок, релиэйбл - не ок
- I'm keen on - я увлекаюсь
- to be - это тот же глагол, что и am, is, are
- do you mind my smoking here - вы не против, если я покурю здесь?
- I'll open the windows if you don't mind - я открою окно, если вы не против
- colleagues - коллеги, коллигс
- educations - эдукэйшн/едукэйшн, и так и сяк ок вроде.
- advertising - эдвёртайзинг, объявления
- chef - чеф, шеф повар
- chief - чиф, шеф/начальник
- complexion - цвет лица
- fabric - ткань
- conductor - дирижёр
- brilliant - блестящий
- constitution - телосложение
- I can't take part - я не могу принять участие
- firstly ... because - в первую очередь ... потому что ...
- secondly - во вторых ...
- thirdly - в третьих
- i feel like GERUND - я хочу
  - having something to eat
  - jogging right now
  - smoking
- plans - плэнс ок, планс не ок
- devote - посвящать
- patronimic name - отчество
- witho'ut
- I'm lucky - мне повезло
- contryside - за город
- negotiation - переговоры
- across river - через реку, а не вдоль реки
- three - зры
- awkward - оквод, не аквад
- butterfingers - неловкий
- boast - хвастаться
- i very like - не ок
- i like very much - ok
- everything is good - не ок
- everything is fine - ok
- courageous - корриджес
- boasty - не ок
- boastful - ок
- amazing - великолепный
- opportunity - возможность
- by road - в дороге
- follow - следить/следовать
- make myself - заставлять себя
- impo'rtant - а не importa'nt
- the most [wonderful|amazing|gorgeous|marvelous] place
- frequently - синонимы лучше подходящие для разговорной речи [often|a lot]
- for breakfast - на завтрак
- I was full after breakfast - объелся
- thankful - благодарен
- it's first time I **came** from moscow to New Your - я впервые в нью йорке
- I'm on business here - я по работе тут
- demand - дема'нд
- to be in great demand - пользоваться хорошим спросом
- young people with competitive skills are in great demand
- dishonored - дизонорд (обесчещенный)
- quick note - записка
- note - заметка
- to set up a meeting - собрать совещание
- yawn - завать, ён
- japanee'se
- mother tongue - родной язык
- speak her mind - говорит то, что у неё на уме
- speak your mind - скажи, что думаешь?
- polite - полайт, а не полит (вежливый)
- put you through - соединить с
- awful - отвратительный
- make a complaint - оставить жалобу
- tongue - танг
- secretary - се'кретери
- figure of speech - фигью оф спич (фраза такая)
