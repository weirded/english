#!/bin/bash

yes=0
no=0
while sleep 0.1; do
	read _ word desc <<< "$(grep '^-' "$1" | shuf -n 1)"
	read -p "$word " know
	if [ "$know" = 'y' -o "$know" = '1' -o "$know" = 'yes' ]; then
		((yes++))
	else
		((no++))
		echo - $desc $word >> bad_words
	fi
	echo "$desc yes=$yes no=$no total=$(python -c "print $yes.0/($yes+$no+0.00001)")"
done
